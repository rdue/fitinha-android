package com.cear.jdnts.fitinha.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.cear.jdnts.fitinha.R;

public class Unlock extends AppCompatActivity {

    private String password = "FIT1603C";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.unlock);

        final EditText pass = (EditText) findViewById(R.id.etPass);
        final Button btPass = (Button) findViewById(R.id.btnPass);
        final TextView tvIncorreto = (TextView) findViewById(R.id.tvIncorreta);

        pass.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        btPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String digPass = pass.getText().toString();
                Log.w("DEBUG", "EditText: " + digPass);

                if ( digPass.equals(password)) {
                    //  Launch app intro
                    Log.w("DEBUG", "Senha Correta");
                    finish();
                    Intent i = new Intent(Unlock.this, Intro.class);
                    startActivity(i);

                    //  Initialize SharedPreferences
                    SharedPreferences getPrefs = PreferenceManager
                            .getDefaultSharedPreferences(getBaseContext());

                    //  Make a new preferences editor
                    SharedPreferences.Editor e = getPrefs.edit();

                    //  Edit preference to make it false because we don't want this to run again
                    e.putBoolean("firstStart", false);

                    //  Apply changes
                    e.apply();


                } else {
                    Log.w("DEBUG", "Senha Incorreta");
                    tvIncorreto.setVisibility(View.VISIBLE);
                }
            }
        });

    }

//    @Override
//    public void onBackPressed() {
//        // do nothing
//    }
}
