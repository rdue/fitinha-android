package com.cear.jdnts.fitinha.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.cear.jdnts.fitinha.activity.MainActivity;


public class BroadcastReceiverAux extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        new NotificationMgmt(context, new Intent(context, MainActivity.class),
                "Fitinha da Barriga", "Fitinha da Barriga", "Hora de efetuar suas medidas!" );

    }
}
