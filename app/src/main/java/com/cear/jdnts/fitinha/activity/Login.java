package com.cear.jdnts.fitinha.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.cear.jdnts.fitinha.R;
import com.cear.jdnts.fitinha.util.ConnectPerfil;
import com.cear.jdnts.fitinha.util.DadosConfig;
import com.cear.jdnts.fitinha.util.SavePerfil;

public class Login extends Fragment {

    private View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.activity_login, container, false);

        // Redireciona para Login() se não estiver logado
        ConnectivityManager connMgr = (ConnectivityManager)
                getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        DadosConfig dadosConfig = new DadosConfig(getContext());

        Log.d("DEBUG", "dadosConfig.getIsLogged(): " + dadosConfig.getIsLogged());

        // Se estiver logado, redireciona para CalcFragment()
        if (dadosConfig.getIsLogged()) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new CalcFragment());
            ft.commit();
        }

        return v;
    }

    @Override
    public void onResume() {

        super.onResume();

        DadosConfig dadosConfig = new DadosConfig(getContext());
        final String fcm = dadosConfig.getFcm();

        final EditText user = (EditText) v.findViewById(R.id.editTextUser);
        final EditText pass = (EditText) v.findViewById(R.id.editTextPass);
        final Button btnLogin = (Button) v.findViewById(R.id.buttonLogin);
        final Button btnCadastro = (Button) v.findViewById(R.id.buttonCadastro);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProgressDialog progress = ProgressDialog.show(getContext(), "Perfil", "Carregando", true);
                ConnectivityManager connMgr = (ConnectivityManager)
                getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {

                    try {
                        String userString = user.getText().toString();
                        String passString = pass.getText().toString();

                        if ( userString != "" && passString != "") {
                            boolean okLogin = new ConnectPerfil(getActivity().getApplicationContext(), getView(), new SavePerfil(0, userString, passString, ""), fcm).LoginPerfil();
                            Log.d("DEBUG", "new DadosConfig(getContext()).getIsLogged(): " + new DadosConfig(getContext()).getIsLogged());


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    Snackbar snackbar = Snackbar
                    .make(v, "Sem conexão com a internet.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        btnCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectivityManager connMgr = (ConnectivityManager)
                        getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {

                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.content_frame, new NewUser());
                    ft.commit();
                } else {
                    Snackbar snackbar = Snackbar
                            .make(v, "Sem conexão com a internet.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });
    }

}
