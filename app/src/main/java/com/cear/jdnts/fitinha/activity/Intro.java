package com.cear.jdnts.fitinha.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.cear.jdnts.fitinha.R;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class Intro extends AppIntro {

    // Please DO NOT override onCreate. Use init.
    @Override
    public void init(Bundle savedInstanceState) {

        // Add your slide's fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.
//        addSlide(new CalcFragment());
//        addSlide(new ConfigFragment());
//        addSlide(new EvoFragment());
//        addSlide(new AboutFragment());

        // Instead of fragments, you can also use our default slide
        // Just set a title, description, background and image. AppIntro will do the rest.
        /*
        TODO: Tutorial de início.
         */
        addSlide(AppIntroFragment.newInstance("", "Para conhecer simples comportamentos que podem diminuir seu risco de desenvolver doenças cardiometabólicas.", R.drawable.logo_fitinha, R.color.ok));
        addSlide(AppIntroFragment.newInstance("", "Além de registrar se está engordando, emagrecendo ou mantendo o peso de acordo com o tamanho da sua cintura.", R.drawable.logo_fitinha, R.color.at));
        addSlide(AppIntroFragment.newInstance("", "Doenças cardiometabólicas envolvem não somente doenças cardiovasculares, diabetes e doença renal crônica.", R.drawable.logo_fitinha, R.color.ok));
        addSlide(AppIntroFragment.newInstance("", "Mas também seus fatores de risco, como obesidade, resistência à insulina, intolerância à glicose, hipertensão e dislipidemia.", R.drawable.logo_fitinha, R.color.at));
        //addSlide(AppIntroFragment.newInstance("Tutorial pg4", "Informações do Tutorial", R.drawable.logo_fitinha, R.color.ok));
//      addSlide(AppIntroFragment.newInstance(title, description, image, background_colour));

        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#2196F3"));

        // Hide Skip/Done button.
        showSkipButton(false);
        setProgressButtonEnabled(true);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permisssion in Manifest.
        setVibrate(true);
        setVibrateIntensity(30);
    }

    @Override
    public void onSkipPressed() {
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed() {
//        moveTaskToBack(true);
        // Do something when users tap on Done button.
        finish();
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void onSlideChanged() {
        // Do something when the slide changes.
    }

    @Override
    public void onNextPressed() {
        // Do something when users tap on Next button.
    }

}