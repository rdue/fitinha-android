package com.cear.jdnts.fitinha.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cear.jdnts.fitinha.activity.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ConnectPerfil {

    private static final String TAG = "DEBUG";
//    private static final String URLlogin = "http://192.168.0.5/fitinha/login.php";
//    private static final String URLcreate = "http://192.168.0.5/fitinha/createUser.php";

    private static final String URLlogin = "http://joaodantas.com/fitinha/login.php";
    private static final String URLcreate = "http://joaodantas.com/fitinha/createUser.php";

    Context context;
    View v;
    SavePerfil savePerfil;
    String fcmToken;
    //int userId = 1;

    private RequestQueue requestQueue;
    private StringRequest request;
    private boolean okLogin = false;

    public ConnectPerfil(Context context, View v, SavePerfil savePerfil, String fcmToken) {
        this.context = context;
        this.v = v;
        this.savePerfil = savePerfil;
        this.fcmToken = fcmToken;
        Log.d(TAG,"Criou a classe");
    }

    public boolean LoginPerfil() {
       // final ProgressDialog progress = ProgressDialog.show(context, "Perfil", "Carregando", true);
        requestQueue = Volley.newRequestQueue(context);
        request = new StringRequest(Request.Method.POST, URLlogin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               // progress.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.has("success")) { //names().get(0).equals("success")){ //Se o primeiro parametro passado for success.. faça isso

                        Toast.makeText(context,jsonObject.getString("success"),Toast.LENGTH_SHORT).show();

                        String email = jsonObject.getJSONObject("user").getString("email");
                        String name = jsonObject.getJSONObject("user").getString("nome");
                        Long id = jsonObject.getJSONObject("user").getLong("_id");

                        new DadosConfig(context).setPerfilDoUsuario(new SavePerfil(id, email, "", name));
                        new DadosConfig(context).setIsLogged(true);

                        okLogin = true;

                        Log.d("DEBUG", "LoginPerfil() : If Success : " + okLogin);


                    } else {
                        Toast.makeText(context, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                        okLogin = false;
                        Log.d("DEBUG", "LoginPerfil() : If error : " + okLogin);
                    }

                    Intent myIntent = new Intent(context, MainActivity.class);
                    myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(myIntent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                hashMap.put("email",savePerfil.getUsername());
                hashMap.put("password",savePerfil.getPassword());
                hashMap.put("fcm", fcmToken);

                return hashMap;
            }
        };

        requestQueue.add(request);
        Log.d("DEBUG", "LoginPerfil() : Return : " + okLogin);
        return okLogin;
    }


    public void CreatePerfil() {
        //final ProgressDialog progress = ProgressDialog.show(context, "Perfil", "Criando Perfil", true);
        requestQueue = Volley.newRequestQueue(context);
        request = new StringRequest(Request.Method.POST, URLcreate, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //progress.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.has("success")){ //names().get(0).equals("success")){ //Se o primeiro paramatro passado for success.. faça isso

                        Toast.makeText(context,jsonObject.getString("success"),Toast.LENGTH_SHORT).show();
                        LoginPerfil();

                    } else {
                        Toast.makeText(context,jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                    Intent myIntent = new Intent(context, MainActivity.class);
                    myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(myIntent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                hashMap.put("email",savePerfil.getUsername());
                hashMap.put("password",savePerfil.getPassword());
                hashMap.put("nome",savePerfil.getNome());

                return hashMap;
            }
        };

        requestQueue.add(request);
    }


}
