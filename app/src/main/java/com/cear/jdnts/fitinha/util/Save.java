package com.cear.jdnts.fitinha.util;


public class Save {
    private long id;
    private double cintura;
    private double altura;
    private String currentTime;
    private int valid;

    public Save(long id, double cintura, double altura, String currentTime) {
        this.id = id;
        this.cintura = cintura;
        this.altura = altura;
        this.currentTime = currentTime;
        this.valid = 1;
    }

    public Save(long id, double cintura, double altura, String currentTime, int valid) {
        this.id = id;
        this.cintura = cintura;
        this.altura = altura;
        this.currentTime = currentTime;
        this.valid = valid;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getCintura() {
        return cintura;
    }

    public void setCintura(double cintura) {
        this.cintura = cintura;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public String getddMM() {
        // yyyy-MM-dd HH:mm:ss
        // 0123456789
        String ddMM = currentTime.substring(8,10) + "/" + currentTime.substring(5,7);
        return ddMM;
    }
}