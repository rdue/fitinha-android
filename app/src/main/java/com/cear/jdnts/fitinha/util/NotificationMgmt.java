package com.cear.jdnts.fitinha.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cear.jdnts.fitinha.R;

/**
 * Created by joao on 17/04/16.
 */
public class NotificationMgmt {

    private Context context;
    private Intent intent;
    private CharSequence ticker;
    private CharSequence titulo;
    private CharSequence descicao;

    public NotificationMgmt(Context context, Intent intent, CharSequence ticker, CharSequence titulo, CharSequence descicao) {
        this.context = context;
        this.intent = intent;
        this.ticker = ticker;
        this.titulo = titulo;
        this.descicao = descicao;

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent p = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setTicker(ticker);
        builder.setContentTitle(titulo);
        builder.setContentText(descicao);
        builder.setSmallIcon(R.drawable.ic_stat_name);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        builder.setContentIntent(p);

        Notification n = builder.build();
        n.vibrate = new long[]{150, 300, 150, 600};
        n.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(R.drawable.ic_stat_name, n);

        Log.d("DEBUG", "Dentro da NotificaçãoMgMt");

        try {
            Uri som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone toque = RingtoneManager.getRingtone(context, som);
            toque.play();
        } catch (Exception e) {
        }
    }

    public void CancelNotification(){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(R.drawable.ic_stat_name);
    }
}
