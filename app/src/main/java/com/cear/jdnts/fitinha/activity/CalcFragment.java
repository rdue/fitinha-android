package com.cear.jdnts.fitinha.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cear.jdnts.fitinha.R;
import com.cear.jdnts.fitinha.util.Calculo;
import com.cear.jdnts.fitinha.util.ConnectData;
import com.cear.jdnts.fitinha.util.DBAdapter;
import com.cear.jdnts.fitinha.util.DadosConfig;
import com.cear.jdnts.fitinha.util.Save;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CalcFragment extends Fragment {
    private View v;
    private DBAdapter dbAdapter;
    private Calculo calculo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_calc, container, false);

        dbAdapter = DBAdapter.getInstance(getActivity());

        // Redireciona para Login() se não estiver logado
        ConnectivityManager connMgr = (ConnectivityManager)
                getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        DadosConfig dadosConfig = new DadosConfig(getContext());

        Log.d("DEBUG", "dadosConfig.getIsLogged(): " + dadosConfig.getIsLogged());

        if ( !dadosConfig.getIsLogged() ) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new Login());
            ft.commit();
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        final EditText altura = (EditText) v.findViewById(R.id.etAltura);
        final EditText cintura = (EditText) v.findViewById(R.id.etCintura);
        final TextView resultado = (TextView) v.findViewById(R.id.tvRes);
        final TextView rce = (TextView) v.findViewById(R.id.tvRCE);
        final Button btnrcest = (Button) v.findViewById(R.id.btnRCEst);
        final ImageButton buttonCalc = (ImageButton) v.findViewById(R.id.buttonCalc);
        final ImageButton buttonSave = (ImageButton) v.findViewById(R.id.buttonSave);
        final ImageButton altBut = (ImageButton) v.findViewById(R.id.buttonAltura);
        final ImageButton cinBut = (ImageButton) v.findViewById(R.id.buttonCintura);
        final ImageButton btSeeTips = (ImageButton) v.findViewById(R.id.buttonSeeTips);
        final LinearLayout resBg = (LinearLayout) v.findViewById(R.id.resLayout);
        final TextView tvResDicas = (TextView) v.findViewById(R.id.tvResDica);

        SharedPreferences sharedPref = getActivity().getSharedPreferences("ALARME_PREF", Context.MODE_PRIVATE);
        if (sharedPref.getBoolean("Trava", false)) {
            altura.setText(String.valueOf(sharedPref.getInt("AltTrava", 0)));
        }

        buttonCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectivityManager connMgr = (ConnectivityManager)
                        getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {

                    DadosConfig dadosConfig = new DadosConfig(getContext());
                    if (dadosConfig.getIsLogged()) {

                        try {
                            double valAlt = Double.parseDouble(altura.getText().toString());
                            double valCin = Double.parseDouble(cintura.getText().toString());

                            if (valAlt == 0 || valCin == 0) {
                                Snackbar snackbar = Snackbar
                                        .make(v, "Insira os valores corretamente.", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            } else {
                                calculo = new Calculo(valAlt, valCin, getContext());
                                resultado.setText(calculo.getSres());
                                resultado.setTextColor(calculo.getFontColor());
                                rce.setText(calculo.getRazao());
                                rce.setTextColor(calculo.getFontColor());
                                btnrcest.setTextColor(calculo.getFontColor());
                                btnrcest.setBackgroundColor(calculo.getColor());
                                resBg.setBackgroundColor(calculo.getColor());
                                tvResDicas.setVisibility(View.VISIBLE);
                                tvResDicas.setText(calculo.getResDica());
                                tvResDicas.setTextColor(calculo.getFontColor());

                                hiddenSave();
                                actionButtonSave();
                            }

                        } catch (NumberFormatException e) {
                            Snackbar snackbar = Snackbar
                                    .make(v, "Insira os valores.", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(v, "Efetue o login.", Snackbar.LENGTH_LONG);
                        snackbar.show();
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.content_frame, new Login());
                        ft.commit();
                    }
                } else  {
                    Snackbar snackbar = Snackbar
                            .make(v, "Sem conexão com a internet.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                actionButtonSave();

            }
        });

        btSeeTips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.alert_tips_cint);
                dialog.setTitle("Dicas para o resultado");
                dialog.setCancelable(true);

                Button btGotIt = (Button) dialog.findViewById(R.id.buttonGotIt);
                btGotIt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });


                dialog.show();
            }
        });

        altBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast toast = Toast.makeText(getContext(), "Digite a sua altura", Toast.LENGTH_LONG);
                toast.show();
            }
        });


        cinBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.alert_tips_cint);
                dialog.setTitle("Como medir a cintura");
                dialog.setCancelable(true);

                Button btGotIt = (Button) dialog.findViewById(R.id.buttonGotIt);
                btGotIt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });


                dialog.show();
            }
        });

        btnrcest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getContext(), "RCEst: Razão Cintura/Estatura.", Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }

    public void actionButtonSave(){
        ConnectivityManager connMgr = (ConnectivityManager)
                getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            DadosConfig dadosConfig = new DadosConfig(getContext());
            if(dadosConfig.getIsLogged()) {

                new AlertDialog.Builder(getContext())
                        .setTitle("Salvar")
                        .setMessage("Deseja salvar esses dados?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Date dt = new Date();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String currentTime = sdf.format(dt);
                                dbAdapter.open();

                                try {
                                    dbAdapter.insert(calculo.getCintura(), calculo.getAltura(), currentTime);
                                    new ConnectData(getContext(), getView(), new Save(1, calculo.getCintura(), calculo.getAltura(), currentTime, 1)).InsertData();

                                } catch (NullPointerException e) {
                                    Snackbar snackbar = Snackbar
                                            .make(v, "Dados incorretos.", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                }

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();


            } else {
                Snackbar snackbar = Snackbar
                        .make(v, "Efetue o login para salvar.", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        } else {
            Snackbar snackbar = Snackbar
                    .make(v, "Sem conexão com a internet.", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    public void hiddenSave(){
        ConnectivityManager connMgr = (ConnectivityManager)
                getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            DadosConfig dadosConfig = new DadosConfig(getContext());
            if (dadosConfig.getIsLogged()) {
                Date dt = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String currentTime = sdf.format(dt);

                try {
                    new ConnectData(getContext(), getView(), new Save(1, calculo.getCintura(), calculo.getAltura(), currentTime, 0)).InsertDataQuiet();
                } catch (NullPointerException e) {
                }
            }
        }
    }
}
