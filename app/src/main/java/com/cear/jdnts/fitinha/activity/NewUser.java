package com.cear.jdnts.fitinha.activity;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cear.jdnts.fitinha.R;
import com.cear.jdnts.fitinha.util.ConnectPerfil;
import com.cear.jdnts.fitinha.util.DadosConfig;
import com.cear.jdnts.fitinha.util.SavePerfil;

public class NewUser extends Fragment {

    private View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.activity_new_user, container, false);

        return v;
    }

    @Override
    public void onResume() {

        super.onResume();

        DadosConfig dadosConfig = new DadosConfig(getContext());
        final String fcm = dadosConfig.getFcm();

        final EditText user = (EditText) v.findViewById(R.id.editTextUser);
        final EditText pass = (EditText) v.findViewById(R.id.editTextPass);
        final EditText passConfirm = (EditText) v.findViewById(R.id.editTextPassConfirm);
        final EditText nome = (EditText) v.findViewById(R.id.editTextNome);
        final Button btnCadastro = (Button) v.findViewById(R.id.buttonCadastro);


        btnCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String userString = user.getText().toString();
                    String passString = pass.getText().toString();
                    String passConfirmString = passConfirm.getText().toString();
                    String nomeString = nome.getText().toString();
                    Log.d("JCFD", "user: " + userString + " pass: " + passString + " passConf: " + passConfirmString + " nome: " + nomeString);

                    if ( userString != "" && passString != "" && passConfirmString != "" && nomeString != "") {
                        if (passString.equals(passConfirmString)) {
                            new ConnectPerfil(getActivity().getApplicationContext(), getView(), new SavePerfil(0, userString, passString, nomeString), fcm).CreatePerfil();
                            ProgressDialog progress = ProgressDialog.show(getContext(), "Perfil", "Carregando", true);
//                            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                            ft.replace(R.id.content_frame, new Login());
//                            ft.commit();
                        } else {
                            Toast toast = Toast.makeText(getContext(), "Digite corretamente as senhas", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

}
