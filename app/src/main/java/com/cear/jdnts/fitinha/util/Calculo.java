package com.cear.jdnts.fitinha.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import com.cear.jdnts.fitinha.R;
import com.cear.jdnts.fitinha.activity.CalcFragment;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Created by joao on 18/02/16.
 */
public class Calculo {
    double razao, cintura, altura, ok, atencao, p1, p2, p3, marg, tam;
    int res;
    String Sres;
    String resDica;
    int fontColor;
    int color;

    private static final int ABX = 1;
    private static final int OK  = 2;
    private static final int ATN = 3;
    private static final int P1  = 4;
    private static final int P2  = 5;
    private static final int P3  = 6;

    private static final Resources resorces = Resources.getSystem();

    public Calculo(double altura, double cintura, Context context) {
        this.cintura = cintura;
        this.altura = altura;

        this.ok = altura * 0.4;
        this.atencao = altura * 0.5;
        this.p1 = altura * 0.6;
        this.p2 = altura * 0.7;
        this.p3 = altura * 0.8;
        this.marg = altura * 0.9;
        this.tam = marg + 4;
        this.razao = cintura / altura;

        if(cintura < ok) {
            res = ABX;
            Sres = "Abaixo do recomendado";
            color = ContextCompat.getColor(context, R.color.white);
            fontColor = ContextCompat.getColor(context, R.color.black);
            resDica = "Inicie ou mantenha contato com médico para ter sua saúde avaliada e adote hábitos mais saudáveis para não agravar sua condição";
        }
        else if (cintura >= ok && cintura < atencao) {
            res = OK;
            Sres = "OK";
            color = ContextCompat.getColor(context, R.color.ok);
            fontColor = ContextCompat.getColor(context, R.color.white);
            resDica = "Mantenha ou aprimore estilo de vida fisicamente ativo e saudável";
        }
        else if (cintura >= atencao && cintura < p1) {
            res = ATN;
            Sres = "Atenção";
            color = ContextCompat.getColor(context, R.color.at);
            fontColor = ContextCompat.getColor(context, R.color.black);
            resDica = "Adote estilo de vida fisicamente ativo e saudável para evitar desenvolvimento de doenças cardiometabólicas";
        }
        else if (cintura >= p1 && cintura < p2 ) {
            res = P1;
            Sres = "Perigo Nivel 1";
            color = ContextCompat.getColor(context, R.color.p1);
            fontColor = ContextCompat.getColor(context, R.color.white);
            resDica = "Inicie ou mantenha contato com médico para ter sua saúde avaliada e adote hábitos mais saudáveis para não agravar sua condição";
        }
        else if (cintura >= p2 && cintura < p3 ) {
            res = P2;
            Sres = "Perigo Nivel 2";
            color = ContextCompat.getColor(context, R.color.p2);
            fontColor = ContextCompat.getColor(context, R.color.white);
            resDica = "Inicie ou mantenha contato com médico para ter sua saúde avaliada e adote hábitos mais saudáveis para não agravar sua condição";
        }
        else if (cintura >= p3) {
            res = P3;
            Sres = "Perigo Nivel 3";
            color = ContextCompat.getColor(context, R.color.p3);
            fontColor = ContextCompat.getColor(context, R.color.white);
            resDica = "Inicie ou mantenha contato com médico para ter sua saúde avaliada e adote hábitos mais saudáveis para não agravar sua condição";
        }
    }

    /**
     * Getter Methods
     * @return
     */

    public double getCintura() {
        return cintura;
    }

    public double getAltura() {
        return altura;
    }

    public String getOk() {
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.DOWN);
        return String.valueOf(df.format(ok));
    }

    public String getAtencao() {
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.DOWN);
        return String.valueOf(df.format(atencao));
    }

    public String getP1() {
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.DOWN);
        return String.valueOf(df.format(p1));
    }

    public String getP2() {
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.DOWN);
        return String.valueOf(df.format(p2));
    }

    public String getP3() {
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.DOWN);
        return String.valueOf(df.format(p3));
    }

    public String getMarg() {
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.DOWN);
        return String.valueOf(df.format(marg));
    }

    public String getTam() {
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.DOWN);
        return String.valueOf(df.format(tam));
    }

    public String getRazao() {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.DOWN);
        return String.valueOf(df.format(razao));
    }

    public int getRes() {
        return res;
    }

    public String getSres() {
        return Sres;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getFontColor() {
        return fontColor;
    }

    public void setFontColor(int fontColor) {
        this.fontColor = fontColor;
    }

    public String getResDica() {
        return resDica;
    }
}