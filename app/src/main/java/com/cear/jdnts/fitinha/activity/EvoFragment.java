package com.cear.jdnts.fitinha.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cear.jdnts.fitinha.R;
import com.cear.jdnts.fitinha.util.DBAdapter;
import com.cear.jdnts.fitinha.util.DadosConfig;
import com.cear.jdnts.fitinha.util.Save;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;


import java.util.ArrayList;
import java.util.List;

public class EvoFragment extends Fragment {
    private View v;
    private DBAdapter dbAdapter;
    private List<Save> saves;
    private BarChart chart;
    private ArrayList<BarEntry> entries;
    private ArrayList<String> labels;
    private BarDataSet dataset;
    private BarData data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_evo, container, false);
        dbAdapter = DBAdapter.getInstance(getActivity());
        chart = (BarChart) v.findViewById(R.id.chart);

        // Redireciona para Login() se não estiver logado
        ConnectivityManager connMgr = (ConnectivityManager)
                getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        DadosConfig dadosConfig = new DadosConfig(getContext());

        Log.d("DEBUG", "dadosConfig.getIsLogged(): " + dadosConfig.getIsLogged());

        if ( !dadosConfig.getIsLogged() ) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new Login());
            ft.commit();
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        addData();
        plotData();

        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

                final int finH = h.getXIndex();

                new AlertDialog.Builder(getContext())
                        .setTitle("Deletar")
                        .setMessage("Deseja deletar esse dado?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    dbAdapter.open();
                                    dbAdapter.deleteItem(saves.get(finH).getId());
                                    Log.w("DEBUG", "Item deletado");
                                    dbAdapter.close();

                                    addData();
                                    dataset.notifyDataSetChanged();
                                    chart.notifyDataSetChanged(); // let the chart know it's data changed
                                    chart.invalidate();
                                    plotData();

                                } catch (NullPointerException f) {
                                    Log.w("DEBUG", "Error Adapter: " + f);
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }

            @Override
            public void onNothingSelected() {
                // Do Nothing
            }
        });

    }

    private void addData() {

        try {
            dbAdapter.open();
            saves = dbAdapter.getAllItem();
            dbAdapter.close();
            Log.w("DEBUG", "Getting all itens");
        } catch (NullPointerException e) {
            Log.w("DEBUG", "Error Adapter: " + e);
        }

        entries = new ArrayList<>();
        labels = new ArrayList<String>();

        for (int i = 0; i < saves.size(); i++) {

            entries.add(new BarEntry((float) saves.get(i).getCintura(), i));
            labels.add(saves.get(i).getddMM());
        }

        dataset = new BarDataSet(entries, "cintura (cm)");
        dataset.setColor(Color.rgb(244, 67, 54));
        data = new BarData(labels, dataset);
    }

    private void plotData() {
        chart.setDescription("");
        chart.getAxisLeft().setDrawLabels(false);
        chart.getAxisRight().setDrawLabels(false);
        //chart.getXAxis().setDrawLabels(false);
        chart.setData(data);
    }
}
