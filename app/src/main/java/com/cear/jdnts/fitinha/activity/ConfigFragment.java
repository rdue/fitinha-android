package com.cear.jdnts.fitinha.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.cear.jdnts.fitinha.R;
import com.cear.jdnts.fitinha.util.Alarm;
import com.cear.jdnts.fitinha.util.DadosConfig;

public class ConfigFragment extends Fragment {

    private SharedPreferences sharedPref = null;
    private Spinner lembrete = null;
    private boolean ControlChanging = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_config, container, false);

        sharedPref = getActivity().getSharedPreferences("ALARME_PREF", Context.MODE_PRIVATE);

        final int alarmeConf = sharedPref.getInt("ALARME", 10);
        int alarmeConfPos = 1;

        if (alarmeConf == 0) {
            alarmeConfPos = 0;
        } else if (alarmeConf == 10) {
            alarmeConfPos = 1;
        } else if (alarmeConf == 15) {
            alarmeConfPos = 2;
        }


        lembrete = (Spinner) v.findViewById(R.id.spinnerLemb);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.lembretespin, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        lembrete.setAdapter(adapter);

        ControlChanging = true;
        lembrete.setSelection(alarmeConfPos);

        final Switch switchAltura = (Switch) v.findViewById(R.id.switchAltura);
        final EditText etAltTrava = (EditText) v.findViewById(R.id.etAltTrava);


        switchAltura.setChecked(sharedPref.getBoolean("Trava", false));
        etAltTrava.setText(String.valueOf(sharedPref.getInt("AltTrava", 0)));


        etAltTrava.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d("TRAVA", "Entrou no setOnFocus");
                if (!hasFocus) {
                    Log.d("TRAVA", "Entrou no setOnFocus e no IF");
                    try {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putInt("AltTrava", Integer.parseInt(etAltTrava.getText().toString()));
                        editor.commit();
                        Log.d("TRAVA", "Altura travada em " + sharedPref.getInt("AltTrava", 0));
                    } catch (NumberFormatException e) {
                        Snackbar snackbar = Snackbar
                                .make(v, "Insira os valores.", Snackbar.LENGTH_LONG);
                        snackbar.show();
                        Log.d("TRAVA", "Entrou no Catch");
                    }
                }
            }
        });

        switchAltura.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean("Trava", true);
                    Log.d("TRAVA", "Trava true");
                    editor.commit();

                } else {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean("Trava", false);
                    Log.d("TRAVA", "Trava false");
                    editor.commit();
                }
            }
        });


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        lembrete.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                boolean b = ControlChanging;

                if (b) {
                    ControlChanging = false;
                    return;
                }


                SharedPreferences.Editor editor = sharedPref.edit();

                switch(position) {
                    case 0:
                        editor.putInt("ALARME", 0);
                        editor.commit();
                        //new Alarm(getContext());
                        Log.d("DEBUG", "Mudou para " +  sharedPref.getInt("ALARME", 0));
                        break;
                    case 1:
                        editor.putInt("ALARME", 10);
                        editor.commit();
                        //new Alarm(getContext());
                        Log.d("DEBUG", "Mudou para " +  sharedPref.getInt("ALARME", 10));
                        break;
                    case 2:
                        editor.putInt("ALARME", 15);
                        editor.commit();
                        //new Alarm(getContext());
                        Log.d("DEBUG", "Mudou para " +  sharedPref.getInt("ALARME", 15));
                        break;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d("DEBUG", "Nada selecionado");
            }
        });

    }

}
