package com.cear.jdnts.fitinha.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class DadosConfig {

    private boolean isFirstTime;
    private String tagIsFirstTime = "firstStart";

    private String fcm = "";

    private SavePerfil perfilDoUsuario;

    private boolean isLogged;
    private String tagIsLogged = "isLoggged";

    private String tagIdUsuario = "idUsuario";
    private String tagNomeUsuario = "nomeUsuario";
    private String tagEmailUsuario = "emailUsuario";

    private String tagFcm = "firebaseToken";

    private Context context;
    // private Dados do Alerme


    public DadosConfig(Context context) {
        this.context = context;
    }

    public boolean getIsFirstTime() {
        //  Initialize SharedPreferences
        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        //  Create a new boolean and preference and set it to true
        isFirstTime = getPrefs.getBoolean(tagIsFirstTime, true);

        return isFirstTime;
    }

    public void setIsFirstTime(boolean firstTime) {
        isFirstTime = firstTime;

        //  Initialize SharedPreferences
        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        //  Create a new boolean and preference and set it to true
        getPrefs.getBoolean(tagIsFirstTime, true);
        //  Make a new preferences editor
        SharedPreferences.Editor e = getPrefs.edit();
        //  Edit preference
        e.putBoolean(tagIsFirstTime, isFirstTime);
        //  Apply changes
        e.apply();

    }

    public SavePerfil getPerfilDoUsuario() {
        //  Initialize SharedPreferences
        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        //  Create a new boolean and preference and set it to true
        Log.d("DEBUG", "getPrefs.getLong(tagIdUsuario, 0) : " + getPrefs.getLong(tagIdUsuario, 0));
        Log.d("DEBUG", "getPrefs.getLong(tagIdUsuario, 0) : " + getPrefs.getLong(tagIdUsuario, 0));
        Log.d("DEBUG", "getPrefs.getLong(tagIdUsuario, 0) : " + getPrefs.getLong(tagIdUsuario, 0));
        Log.d("DEBUG", "getPrefs.getLong(tagIdUsuario, 0) : " + getPrefs.getLong(tagIdUsuario, 0));



        perfilDoUsuario = new SavePerfil(getPrefs.getLong(tagIdUsuario, 0), //id
                getPrefs.getString(tagEmailUsuario, ""), //email
                "", //senha
                getPrefs.getString(tagNomeUsuario, "")); //nome
        return perfilDoUsuario;
    }

    public void setPerfilDoUsuario(SavePerfil perfilDoUsuario) {
        this.perfilDoUsuario = perfilDoUsuario;

        //  Initialize SharedPreferences
        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        //  Create a new boolean and preference and set it to true
        getPrefs.getLong(tagIdUsuario, 0);
        getPrefs.getString(tagEmailUsuario, "");
        getPrefs.getString(tagNomeUsuario, "");
        //  Make a new preferences editor
        SharedPreferences.Editor e = getPrefs.edit();
        //  Edit preference
        e.putLong(tagIdUsuario, perfilDoUsuario.getId());
        e.putString(tagNomeUsuario, perfilDoUsuario.getNome());
        e.putString(tagEmailUsuario, perfilDoUsuario.getUsername());
        //  Apply changes
        e.apply();

    }

    public boolean getIsLogged() {
        //  Initialize SharedPreferences
        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        //  Create a new boolean and preference and set it to true
        isLogged = getPrefs.getBoolean(tagIsLogged, false);
        return isLogged;
    }

    public void setIsLogged(boolean logged) {
        isLogged = logged;
        Log.d("DEBUG", "DadosConfig : setIsLogged : isLogged : " + isLogged);

        //  Initialize SharedPreferences
        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        //  Create a new boolean and preference and set it to true
        getPrefs.getBoolean(tagIsLogged, false);
        //  Make a new preferences editor
        SharedPreferences.Editor e = getPrefs.edit();
        //  Edit preference
        e.putBoolean(tagIsLogged, isLogged);
        //  Apply changes
        e.apply();

        Log.d("DEBUG", "DadosConfig : setIsLogged : isLogged after Apply: " + isLogged);

    }

    public String getFcm() {
        //  Initialize SharedPreferences
        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        //  Create a new boolean and preference and set it to true
        fcm = getPrefs.getString(tagFcm, "");

        return fcm;
    }

    public void setFcm(String recent_fcm) {
        fcm = recent_fcm;

        //  Initialize SharedPreferences
        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        //  Create a new boolean and preference and set it to true
        getPrefs.getString(tagFcm, "");
        //  Make a new preferences editor
        SharedPreferences.Editor e = getPrefs.edit();
        //  Edit preference
        e.putString(tagFcm, fcm);
        //  Apply changes
        e.apply();

    }
}
