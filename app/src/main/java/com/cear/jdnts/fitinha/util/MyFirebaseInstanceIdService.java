package com.cear.jdnts.fitinha.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by joao on 17/09/17.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String REG_TOKEN = "REG_TOKEN";
    @Override
    public void onTokenRefresh() {
        Log.d("JCFD", "Olha o token ai");
        String recent_token = FirebaseInstanceId.getInstance().getToken();
        DadosConfig dadosConfig = new DadosConfig(getApplicationContext());
        dadosConfig.setFcm(recent_token);

        Log.d(REG_TOKEN, recent_token);
    }
}
