package com.cear.jdnts.fitinha.activity;


import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.cear.jdnts.fitinha.R;
import com.cear.jdnts.fitinha.util.ConnectMsg;
import com.cear.jdnts.fitinha.util.DadosConfig;
import com.cear.jdnts.fitinha.util.Msg;

import java.util.ArrayList;

public class MsgFragment extends Fragment {

    private View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_msg, container, false);

        // Redireciona para Login() se não estiver logado
        ConnectivityManager connMgr = (ConnectivityManager)
                getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        DadosConfig dadosConfig = new DadosConfig(getContext());

        Log.d("DEBUG", "dadosConfig.getIsLogged(): " + dadosConfig.getIsLogged());

        if ( !dadosConfig.getIsLogged() ) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new Login());
            ft.commit();
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d("DEBUG", "MsgFragment(): OnResume");

        final EditText msg = (EditText) v.findViewById(R.id.etMsg);
        final Button btnEnviar = (Button) v.findViewById(R.id.enviarMsg);

        ConnectMsg PopulateMsg = new ConnectMsg(getActivity(), v, "");
        PopulateMsg.GetMsg();

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ConnectivityManager connMgr = (ConnectivityManager)
                        getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {

                    DadosConfig dadosConfig = new DadosConfig(getContext());
                    if(dadosConfig.getIsLogged()) {

                        new AlertDialog.Builder(getContext())
                                .setTitle("Enviar")
                                .setMessage("Deseja enviar esta mensagem?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        try {
                                            new ConnectMsg(getContext(), getView(), msg.getText().toString()).InsertMsg();
                                            msg.setText("");


                                        } catch (NullPointerException e) {
                                            Snackbar snackbar = Snackbar
                                                    .make(v, "Dados incorretos.", Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }

                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();


                    } else {
                        Snackbar snackbar = Snackbar
                                .make(v, "Efetue o login para enviar.", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }

                } else {
                    Snackbar snackbar = Snackbar
                            .make(v, "Sem conexão com a internet.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }


            }
        });
    }
}
