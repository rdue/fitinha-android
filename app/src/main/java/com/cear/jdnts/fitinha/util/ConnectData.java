package com.cear.jdnts.fitinha.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cear.jdnts.fitinha.R;
import com.cear.jdnts.fitinha.activity.NewUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class ConnectData {

    private static final String TAG = "DEBUG";
    private static final String URLinsertData = "http://joaodantas.com/fitinha/insertData.php";
//    private static final String URLinsertData = "http://192.168.0.5/fitinha/insertData.php";


    Context context;
    View v;
    Save saveDados;
    //int userId = 1;

    private RequestQueue requestQueue;
    private StringRequest request;
    private boolean okLogin = false;

    public ConnectData(Context context, View v, Save saveDados) {
        this.context = context;
        this.v = v;
        this.saveDados = saveDados;
        Log.d(TAG,"Criou a classe de dados");
    }

    public boolean InsertData() {
        final ProgressDialog progress = ProgressDialog.show(context, "Dados", "Inserindo dados.", true);
        requestQueue = Volley.newRequestQueue(context);
        request = new StringRequest(Request.Method.POST, URLinsertData, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progress.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.has("success") ) { // names().get(0).equals("success")){ //Se o primeiro parametro passado for success.. faça isso

                        Toast.makeText(context,jsonObject.getString("success"),Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                hashMap.put("alt", String.valueOf(saveDados.getAltura()));
                hashMap.put("cint", String.valueOf(saveDados.getCintura()));

                DadosConfig dadosConfig = new DadosConfig(context);
                SavePerfil savePerfil = dadosConfig.getPerfilDoUsuario();

                hashMap.put("userId", String.valueOf(savePerfil.getId()));
                hashMap.put("valid", String.valueOf(saveDados.getValid()));

                return hashMap;
            }
        };

        requestQueue.add(request);
        return okLogin;
    }

    public boolean InsertDataQuiet() {
        requestQueue = Volley.newRequestQueue(context);
        request = new StringRequest(Request.Method.POST, URLinsertData, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                hashMap.put("alt", String.valueOf(saveDados.getAltura()));
                hashMap.put("cint", String.valueOf(saveDados.getCintura()));

                DadosConfig dadosConfig = new DadosConfig(context);
                SavePerfil savePerfil = dadosConfig.getPerfilDoUsuario();

                hashMap.put("userId", String.valueOf(savePerfil.getId()));
                hashMap.put("valid", String.valueOf(saveDados.getValid()));

                return hashMap;
            }
        };

        requestQueue.add(request);
        return okLogin;
    }

}
