package com.cear.jdnts.fitinha.activity;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cear.jdnts.fitinha.R;
import com.cear.jdnts.fitinha.util.DadosConfig;
import com.cear.jdnts.fitinha.util.SavePerfil;

public class Perfil extends Fragment {

    private View v;
    private boolean isLogged;
    private TextView nome;
    private TextView email;
    private Button editar;
    private Button sair;
    private SavePerfil perfilDoUsuario;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.activity_perfil, container, false);

        nome = (TextView) v.findViewById(R.id.tvNomeUsuario);
        email = (TextView) v.findViewById(R.id.tvEmailUsuario);
        editar = (Button) v.findViewById(R.id.buttonEditarUsuario);
        sair = (Button) v.findViewById(R.id.buttonSairUsuario);

        isLogged = new DadosConfig(getContext()).getIsLogged();
        Log.d("DEBUG", "Ta Logado? " + isLogged);

        if( !isLogged ) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new Login());
            ft.commit();
        }
        return v;
    }



    @Override
    public void onResume() {
        super.onResume();

        isLogged = new DadosConfig(getContext()).getIsLogged();
        Log.d("DEBUG", "Ta Logado? " + isLogged);

        if( !isLogged ) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new Login());
            ft.commit();
        } else {
            DadosConfig dadosConfig = new DadosConfig(getContext());
            perfilDoUsuario = dadosConfig.getPerfilDoUsuario();
            nome.setText(perfilDoUsuario.getNome());
            email.setText(perfilDoUsuario.getUsername());

        }

        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DadosConfig(getContext()).setIsLogged(false);
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, new CalcFragment());
                ft.commit();
            }
        });

    }

}
