package com.cear.jdnts.fitinha.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cear.jdnts.fitinha.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by joao on 22/05/17.
 */

public class ConnectMsg {


    private static final String TAG = "DEBUG";
    private static final String URLinsertMsg = "http://joaodantas.com/fitinha/insertMsg.php";
    //private static final String URLinsertMsg = "http://192.168.0.5/fitinha/insertMsg.php";
    //10.0.2.2
    private static final String URLgetMsg = "http://joaodantas.com/fitinha/getMsg.php";
    //private static final String URLgetMsg = "http://192.168.0.5/fitinha/getMsg.php";



    ArrayAdapter<String> adapter;
    ArrayList<String> itemsPopulate;

    Context context;
    View v;
    String mensagem;

    private RequestQueue requestQueue;
    private StringRequest request;
    private boolean okLogin = false;

    public ConnectMsg(Context context, View v, String mensagem) {
        this.context = context;
        this.v = v;
        this.mensagem = mensagem;

    }

    public boolean InsertMsg() {
        final ProgressDialog progress = ProgressDialog.show(context, "Mensagens", "Enviando", true);
        requestQueue = Volley.newRequestQueue(context);
        request = new StringRequest(Request.Method.POST, URLinsertMsg, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progress.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.has("success") ) { // names().get(0).equals("success")){ //Se o primeiro parametro passado for success.. faça isso

                        Toast.makeText(context,jsonObject.getString("success"),Toast.LENGTH_SHORT).show();
                        okLogin = true;
                        ConnectMsg PopulateMsg = new ConnectMsg(context, v, "");
                        PopulateMsg.GetMsg();

                        //String msg = jsonObject.getJSONObject("user").getString("email");

                    } else {
                        Toast.makeText(context, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override // Parâmetros passados no POST
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                hashMap.put("msg", mensagem);

                DadosConfig dadosConfig = new DadosConfig(context);
                SavePerfil savePerfil = dadosConfig.getPerfilDoUsuario();

                hashMap.put("userId", String.valueOf(savePerfil.getId()));
                hashMap.put("tipo", "1");

                return hashMap;
            }
        };

        requestQueue.add(request);
        return okLogin;
    }


    public boolean GetMsg() {
        final ProgressDialog progress = ProgressDialog.show(context, "Mensagens", "Carregando", true);

        final ListView listView = (ListView) v.findViewById(R.id.lvMsg);
        final ArrayList<Msg> items = new ArrayList<Msg>();
        requestQueue = Volley.newRequestQueue(context);
        request = new StringRequest(Request.Method.POST, URLgetMsg, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progress.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.has("success")) { //names().get(0).equals("success")){ //Se o primeiro parametro passado for success.. faça isso

                        Toast.makeText(context,jsonObject.getString("success"),Toast.LENGTH_SHORT).show();
                        okLogin = true;

                        for (int i = 0; i < jsonObject.getJSONArray("result").length(); i++) {
                            int userId = jsonObject.getJSONArray("result").getJSONObject(i).getInt("userId");
                            int tipo = jsonObject.getJSONArray("result").getJSONObject(i).getInt("tipo");
                            String msg = jsonObject.getJSONArray("result").getJSONObject(i).getString("msg"); //jsonObject.getJSONObject("result").getString("msg");
                            int id = jsonObject.getJSONArray("result").getJSONObject(i).getInt("_id");
                            String date = jsonObject.getJSONArray("result").getJSONObject(i).getString("date");
                            int readed = jsonObject.getJSONArray("result").getJSONObject(i).getInt("readed");
                            items.add(new Msg(msg, userId, id, tipo, date, readed));
                            Log.d("DEBUG", "ConectMsg() - msg:" + msg + " userId: " + userId + " id: " + id + " tipo: " + tipo + " date: " + date);
                        }
                        Log.d("DEBUG", "ConnectMsg() : items.size() " + items.size());
                        itemsPopulate = new ArrayList<String>();
                        for (int i = 0; i < items.size(); i++) {
                            itemsPopulate.add(items.get(i).getMsg());
                        }

                        adapter = new ArrayAdapter(context, R.layout.simplelist_1_layout, R.id.tv, itemsPopulate);
                        listView.setAdapter(adapter);
                        //Mudando o fundo da mensagem não lida

                        for (int i = 0; i < items.size(); i++) {
                            if( items.get(i).getReaded() == 1) {
                                listView.performItemClick(
                                        listView.getAdapter().getView(i, null, null), i, i);
                                listView.requestFocusFromTouch();
                                listView.setSelection(i);
                            }
                        }


                    } else {
                        Toast.makeText(context, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                        okLogin = false;
                        //Log.d("DEBUG", "LoginPerfil() : If error : " + okLogin);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override // Parâmetros passados no POST
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> hashMap = new HashMap<String, String>();

                DadosConfig dadosConfig = new DadosConfig(context);
                SavePerfil savePerfil = dadosConfig.getPerfilDoUsuario();

                hashMap.put("userId", String.valueOf(savePerfil.getId()));

                return hashMap;
            }
        };

        requestQueue.add(request);
        return okLogin;
    }

}