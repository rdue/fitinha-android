package com.cear.jdnts.fitinha.util;

/**
 * Created by joao on 22/05/17.
 */

public class Msg {

    private String msg;
    private int userId;
    private int id;
    private int tipo;
    private String date;
    private int readed;

    public Msg(String msg, int userId, int id, int tipo, String date, int readed) {
        this.msg = msg;
        this.userId = userId;
        this.id = id;
        this.tipo = tipo;
        this.date = date;
        this.readed = readed;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setReaded(int readed) {
        this.readed = readed;
    }

    public int getReaded() {
        return readed;
    }



}
