package com.cear.jdnts.fitinha.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Calendar;

public class Alarm {

    private final SharedPreferences sharedPref;
    private final Context context;

    public Alarm(Context context) {

        Log.d("ALARM", "Criou a classe Alarm");

        this.context = context;
        sharedPref = context.getSharedPreferences("ALARME_PREF", Context.MODE_PRIVATE);

        boolean alarmeAtivo = (PendingIntent.getBroadcast(context, 0,
                new Intent("ALARME_DISPARO"), PendingIntent.FLAG_NO_CREATE) == null);


        int alarmeConf = sharedPref.getInt("ALARME", 10);

        Log.d("ALARM", "Entrou no if");
        Intent intent = new Intent("ALARME_DISPARO");
        PendingIntent p = PendingIntent.getBroadcast(context, 0, intent, 0);
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        c.add(Calendar.HOUR_OF_DAY, 14);


        if (alarmeConf == 0) {
            AlarmCancel(context);
            Log.d("ALARM", "Alarme cancelado");
        } else {
            AlarmManager alarme = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarme.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(),
                    alarmeConf/*Dias*/ * 24/*Horas*/ * 60/*Min*/ * 60/*Sec*/ * 1000,
                    p);
            Log.d("ALARM", "Alarme setado para " + sharedPref.getInt("ALARME", 10) + " dias");
        }


    }

    public void AlarmCancel(Context context) {
        Intent intent = new Intent("ALARME_DISPARO");
        PendingIntent p = PendingIntent.getBroadcast(context, 0, intent, 0);

        AlarmManager alarme = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarme.cancel(p);
    }
}
