package com.cear.jdnts.fitinha.util;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by joao on 01/02/16.
 */
public class DBAdapter {
    public static final String KEY_ROWID = "_id";
    public static final String KEY_CINTURA = "cintura";
    public static final String KEY_ALTURA = "altura";
    public static final String KEY_DATE = "date";
    private static final String TAG = "DBAdapter";
    private static final String DATABASE_NAME = "MyDB";
    private static final String DATABASE_TABLE = "DBalt";
    private static final int DATABASE_VERSION = 3;

    private static final String DATABASE_CREATE =
            "create table "
                    + DATABASE_TABLE +" ( "
                    + KEY_ROWID + " integer primary key, "
                    + KEY_CINTURA + " real not null, "
                    + KEY_ALTURA + " real not null, "
                    + KEY_DATE + " datetime ); ";

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    private static DBAdapter mInstance = null;
    /**
     * Método para criação de uma instancia do banco de dados segundo o padrão Singleton.
     * @param context
     * @return DBAdapter
     */
    public static DBAdapter getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DBAdapter(context.getApplicationContext());
        }
        return mInstance;
    }

    public DBAdapter(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    /**
     * Classe DatabaseHelper que herda SQLiteOpenHelper, necessário para criação do BD
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db){
            try {
                db.execSQL(DATABASE_CREATE);
                Log.w(TAG, "Banco de Dados criado com sucesso!");
            } catch (SQLException e){
                e.printStackTrace();
                Log.w(TAG, "Erro ao criar o banco de dados");
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
            Log.w(TAG, "Atualizando a base de dados da versão " + oldVersion + " para a "
                    + newVersion + ", o que destruirá todos os dados antigos");
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(db);	// Chama o método de criação de BD.
        }
    }


    /**
     * Método de abertura da base de dados.
     * @return DBAdapter
     * @throws SQLException
     */
    public DBAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    /**
     * Método para fechar a base de dados.
     */
    public void close() {
        DBHelper.close();
    }

    /**
     * Método que destroi a base de dados.
     */
    public void drop(){
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
    }

    /**
     * Método que insere um item a base de dados
     * @param cintura
     * @param altura
     * @return long
     */
    public long insert(double cintura, double altura, String date){
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_CINTURA, cintura);
        initialValues.put(KEY_ALTURA, altura);
        initialValues.put(KEY_DATE, date);
        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    /**
     * Método que deleta um item.
     * @param rowId
     * @return boolean
     */
    public boolean deleteItem(long rowId){
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    /**
     * Método que retorna todos os itens.
     * @return ArrayList<Item>
     */
    public ArrayList<Save> getAllItem(){
        Cursor mCursor = db.query(DATABASE_TABLE, new String[]{KEY_ROWID, KEY_CINTURA, KEY_ALTURA, KEY_DATE}, null, null, null, null, null);
        ArrayList<Save> itemArray = new ArrayList<Save> ();
        for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
            Save save = new Save(
                    mCursor.getLong(0),
                    mCursor.getDouble(1),
                    mCursor.getDouble(2),
                    mCursor.getString(3) );
            itemArray.add(save);
        }
        mCursor.close();
        return itemArray;
    }

    /**
     * Método que Retorna um item em particular.
     * @param rowId
     * @return Cursor
     * @throws SQLException
     */
    public Save getItem(long rowId) throws SQLException{
        Cursor mCursor = db.query(true, DATABASE_TABLE, new String[] {KEY_ROWID, KEY_CINTURA, KEY_ALTURA, KEY_DATE},
                KEY_ROWID + "=" + rowId, null, null, null, null, null);
        if (mCursor != null){
            mCursor.moveToFirst();
        }

        Save save = new Save(
                mCursor.getLong(0),
                mCursor.getDouble(1),
                mCursor.getDouble(2),
                mCursor.getString(3) );
        mCursor.close();

        return save;
    }

}